#include <iostream>
#include "Image.hpp"

int main()
{
    Image img(40,30);
    remplir(img);
    Image img2 = bordure(img,127);
    ecrirePnm(img, "truc.pnm");

    // ATTENTION
    // Image i2 = i1 ET Image i2(i1) ideù, c'est une copie
    // MAIS i2 = i1 c'est une affectation avec l'operator=
    // Donc operator=
    //      Image img2(200,150);
    //      img= img2
    //      --> Ca bug
    //  Mais pas Image img2 = img;

    return 0;
}
