#include <CppUTest/CommandLineTestRunner.h>
#include "Image.hpp"

int main(int argc, char ** argv) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}

