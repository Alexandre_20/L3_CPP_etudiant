#include "Ligne.hpp"
#include <iostream>

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1):
	FigureGeometrique(couleur), _p0(p0), _p1(p1) {}
	
void Ligne::afficher() const
{
	std::cout << "Ligne " << this->getCouleur()._r << "_" << 
	this->getCouleur()._g << "_" << this->getCouleur()._b << " " << 
	this->getP0()._x << "_" << this->getP0()._y << " " << 
	this->getP1()._x << "_" << this->getP1()._y << std::endl;
}

const Point & Ligne::getP0() const
{
	return _p0;
}


const Point & Ligne::getP1() const
{
	return _p1;
}
