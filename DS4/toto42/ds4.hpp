#ifndef DS4_HPP
#define DS4_HPP

class Personne {
	protected:
		std::string _nom;
	public:
		Personne(const std::string & nom) : _nom(nom) {}
		void afficher() const {
			std::cout << _nom << std::endl;
		}
};

class Etudiant : public Personne {
	private:
		float _moyenne;
	public:
		Etudiant(const std::string & nom, float moyenne):
			Personne(nom), _moyenne(moyenne) {}
		void afficher() const {
			std::cout << _nom << ";" << _moyenne << std::endl;
		}
};

class Prof : public Personne {
    private:
        float _salaire;
    public:
        Prof(const std::string & nom, float salaire) :
            Personne(nom), _salaire(salaire) {}
        void afficher() const override {
            std::cout << _nom << ";" << _salaire << std::endl;
        }
};
#endif
