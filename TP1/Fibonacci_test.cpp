#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci){};

TEST(GroupFibonacci, test_fibonacciIteratif){
    int result = fibonacciIteratif(1);
    int result1 = fibonacciIteratif(2);
    int result2 = fibonacciIteratif(3);
    int result3 = fibonacciIteratif(4);
    int result4 = fibonacciIteratif(5);

    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, result1);
    CHECK_EQUAL(1, result2);
    CHECK_EQUAL(2, result3);
    CHECK_EQUAL(3, result4);
}

TEST(GroupFibonacci, test_fibonacciRecursif){
    int result = fibonacciRecursif(1);
    int result1 = fibonacciRecursif(2);
    int result2 = fibonacciRecursif(3);
    int result3 = fibonacciRecursif(4);
    int result4 = fibonacciRecursif(5);

    CHECK_EQUAL(0, result);
    CHECK_EQUAL(1, result1);
    CHECK_EQUAL(1, result2);
    CHECK_EQUAL(2, result3);
    CHECK_EQUAL(3, result4);
}
