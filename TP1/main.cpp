// DEWAELE Audrey - FOVET Alexandre

#include <iostream>
#include "Fibonacci.hpp"
#include "vecteur3.hpp"

int main()
{
	std::cout << fibonacciIteratif(7) << std::endl;
	std::cout << fibonacciRecursif(7) << std::endl;
	
    vecteur temp;
    temp.x = 2;
    temp.y = 3;
    temp.z = 6;

	return 0;
}
