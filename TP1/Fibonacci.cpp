// DEWAELE Audrey - FOVET Alexandre
// Fibonacci.cpp

#include <iostream>
#include "Fibonacci.hpp"

int fibonacciIteratif(int x)
{
	int temp = 0;
    int temp2 = 1;
	int res = 0;
	
    if(x==1) return temp;
    if(x==2) return temp2;
	
	for(int i = 1; i < x-1; i++)
	{
		res = temp + temp2 ;
		temp = temp2;
		temp2 = res; 
		//std::cout << res << " " ; 
	}
	return res; 
}
	
int fibonacciRecursif(int x)
{
	if(x==1) return 0;
	if(x==2) return 1;
	
	return fibonacciRecursif(x-1) + fibonacciRecursif(x-2) ;		
}

