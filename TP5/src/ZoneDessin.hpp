#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP

#include "FigureGeometrique.hpp"
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include <vector>
#include <gtkmm.h>

class ZoneDessin : public Gtk::DrawingArea
{
    private:
        std::vector<FigureGeometrique*> _figures;

    public:
        ZoneDessin();
        ~ZoneDessin() override;

    protected:
        // bool on_draw(GdkEventExpose* event );
        bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;
        bool gererClic( GdkEventButton* event);

};

#endif // ZONEDESSIN_HPP
