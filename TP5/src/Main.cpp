#include <iostream>
#include <vector>
#include <memory>

#include <gtkmm.h>

#include "Ligne.hpp"
#include "Couleur.hpp" 
#include "Point.hpp"
#include "FigureGeometrique.hpp"
#include "PolygoneRegulier.hpp"
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

using namespace std;


int main(int argc, char ** argv)
{
    /*
	//Couleur c(1,0,0);
	Couleur c;
	c._r = 0;
	c._g = 1;
	c._b = 0;
	
	//Point a(50,100);
	//Point b(100,200);
	Point a;
	a._x = 50;
	a._y = 100;
	
	Point b;
	b._x = 100;
	b._y = 200;
	
	Ligne l(c,a,b);
	
	//l.afficher();
	
	Point centre;
	centre._x = 100;
	centre._y = 200;
	
	PolygoneRegulier pr(c,centre,50,5);
    //pr.afficher();
    */
	/*
	std::vector <FigureGeometrique*> vec;
	vec.push_back(new PolygoneRegulier(c,centre,50,5));
	vec.push_back(new Ligne(c,a,b));
	
	for( auto x : vec)
	{
		std::cout << "AFFICHER" << std::endl;
		x->afficher();
	}*/
    /*
	vector <unique_ptr<FigureGeometrique>> vec;
	
	vec.push_back(make_unique <PolygoneRegulier>(c,centre,50,5));
	vec.push_back(make_unique <Ligne>(c,a,b));
	
	for(const auto& x : vec)
	{
		x->afficher();
		cout << endl << endl ;
    }
    */
	
    /*
    // Une fenêtre

    Gtk::Main kit(argc, argv);              // application gtkmm
    Gtk::Window window;                     // fenetre principale
    Gtk::Label label(" Hello world ! ");    // message
    window.add(label);
    window.show_all();
    kit.run(window); */

    ViewerFigures ma_fenetre(argc, argv);
    ma_fenetre.run();

    return 0;
}    

