#include "PolygoneRegulier.hpp"
#include <iostream>
#include <cmath>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):
	FigureGeometrique(couleur), _nbPoints(nbCotes)
	{
		_points = new Point [nbCotes];
		Point temp;
		float alpha;
		
		for(int i = 0; i < _nbPoints; i++)
		{
			alpha = ( i * M_PI *2) / _nbPoints ;
			temp._x = rayon * cos(alpha) + centre._x;
			temp._y = rayon * sin(alpha) + centre._y;
			_points[i] = temp;
		}
		
		
}

void PolygoneRegulier::afficher(const
Cairo::RefPtr<Cairo::Context> & context) const
{
    const Couleur & c = getCouleur();
    context->set_source_rgb(c._r, c._g, c._b);

    const Point &p1 = _points[_nbPoints-1];
    context->move_to(p1._x, p1._y);

    for (int i=0; i<_nbPoints; i++)
    {
        const Point & p = _points[i];
        context->line_to(p._x, p._y);
    }

    context->stroke();
}

    /*
	std::cout << "PolyReg " << this->getCouleur()._r << "_" << 
	this->getCouleur()._g << "_" << this->getCouleur()._b << " ";
	
	for(int i = 0; i < _nbPoints; i++)
	{
		std::cout<< _points[i]._x << "_" <<_points[i]._y << " ";
    }
	
}*/

int PolygoneRegulier::getNbPoints() const
{
	return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const
{
	return _points[indice] ;
}

PolygoneRegulier::~PolygoneRegulier()
{
	delete[] _points;
}	
	
