#ifndef FigureGeometrique_HPP_
#define FigureGeometrique_HPP_

#include <iostream>
#include <gtkmm.h>
#include "Couleur.hpp"

class FigureGeometrique
{
	protected:
		Couleur _couleur;
		
	public:
		FigureGeometrique(const Couleur & couleur);
		const Couleur & getCouleur() const;
        virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const = 0 ;

};
#endif
