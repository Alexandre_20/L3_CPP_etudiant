cmake_minimum_required( VERSION 3.0 )
project( TP5 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
pkg_check_modules( PKG REQUIRED gtkmm-3.0 cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0)

link_directories( 
	${GTKMM_LIBRARY_DIRS} )

include_directories(
	${GTKMM_INCLUDE_DIRS} )

# programme principal
add_executable( Main.out src/Main.cpp 
    src/Ligne.cpp src/FigureGeometrique.cpp src/PolygoneRegulier.cpp src/ViewerFigures.cpp src/ZoneDessin.cpp)
target_link_libraries( Main.out 
	${GTKMM_LIBRARIES} )

